package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Flywheels;

public class SpinFlywheels extends CommandBase {

    private final Flywheels flywheels;
    public SpinFlywheels(Flywheels flywheels) {
        this.flywheels = flywheels;
        
        addRequirements(this.flywheels);
    }
    @Override
    public void execute() {
        flywheels.setSpeed(flywheels.getTopSetpointRPM(), flywheels.getBottomSetpointRPM());
    }

    @Override
    public boolean isFinished() {
        return flywheels.isWithinTolerance();
    }
}

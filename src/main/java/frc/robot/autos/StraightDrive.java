
package frc.robot.autos;

import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import frc.robot.subsystems.Swerve;


public class StraightDrive extends SequentialCommandGroup{
    public StraightDrive(Swerve swerve) {
        super(
            new InstantCommand(() -> swerve.zeroGyro()),
            new InstantCommand(() -> swerve.drive(new Translation2d(3, new Rotation2d()), 0, true)),
            new WaitCommand(10)
        );
    }
}

package frc.robot.subsystems;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMax.IdleMode;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants.IndexerConstants;

public class Indexer extends CommandBase {

    private CANSparkMax indexerMotor;

    public Indexer() {
        indexerMotor = new CANSparkMax(IndexerConstants.INDEXER_MOTOR_ID, MotorType.kBrushless);

        indexerMotor.restoreFactoryDefaults();

        indexerMotor.setInverted(false);

        indexerMotor.setIdleMode(IdleMode.kBrake);

    }

    public void setRawSpeed(double speed) {
        indexerMotor.set(speed);
    }

    public void stopIndexer() {
        indexerMotor.stopMotor();
    }


    
}
 
package frc.robot.subsystems;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMax.ControlType;
import com.revrobotics.CANSparkMax.IdleMode;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;
import com.revrobotics.SparkMaxPIDController;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants.FlywheelConstants;

public class Flywheels extends SubsystemBase {

    private CANSparkMax topFlywheel;
    private SparkMaxPIDController topFlywheelPIDController;
    private CANSparkMax bottomFlywheel;
    private SparkMaxPIDController bottomFlywheelPIDController;

    private final double RPM_TOLERANCE = 50;

    // private double topFeedForward;
    // private double bottomFeedForward;

    private final double TOP_SETPOINT_RPM = 3000;
    private final double BOTTOM_SETPOINT_RPM = 3000;

    public Flywheels() {
        topFlywheel = new CANSparkMax(FlywheelConstants.TOP_MOTOR_ID, MotorType.kBrushless);
        bottomFlywheel = new CANSparkMax(FlywheelConstants.BOTTOM_MOTOR_ID, MotorType.kBrushless);

        topFlywheel.restoreFactoryDefaults();
        bottomFlywheel.restoreFactoryDefaults();

        topFlywheelPIDController = topFlywheel.getPIDController();
        bottomFlywheelPIDController = bottomFlywheel.getPIDController();


        topFlywheelPIDController.setFF(.0001565); // changed
		topFlywheelPIDController.setP(0.00015); // changed
		topFlywheelPIDController.setI(0.0000001);
		topFlywheelPIDController.setD(0.0007);


        bottomFlywheelPIDController.setFF(.0001685); // changed
		bottomFlywheelPIDController.setP(0.00015);
		bottomFlywheelPIDController.setI(0.0000001);
		bottomFlywheelPIDController.setD(0.0007);

        // topFlywheelPIDController = new ProfiledPIDController(0.002, 0, 0.0003, new Constraints(80, 160));
        // bottomFlywheelPIDController = new ProfiledPIDController(0.002, 0, 0.0003, new Constraints(80, 160));

        // topFlywheelPIDController.setTolerance(RPM_TOLERANCE);
        // bottomFlywheelPIDController.setTolerance(RPM_TOLERANCE);

        topFlywheel.setInverted(true);
        bottomFlywheel.setInverted(false);

        // topFeedForward = 0;
        // bottomFeedForward = 0;

        SmartDashboard.putNumber("smallP", topFlywheelPIDController.getP());
        SmartDashboard.putNumber("smallI", topFlywheelPIDController.getI());
        SmartDashboard.putNumber("smallD", topFlywheelPIDController.getD());
        SmartDashboard.putNumber("smallF", topFlywheelPIDController.getFF());

        SmartDashboard.putNumber("smallP", bottomFlywheelPIDController.getP());
        SmartDashboard.putNumber("smallI", bottomFlywheelPIDController.getI());
        SmartDashboard.putNumber("smallD", bottomFlywheelPIDController.getD());
        SmartDashboard.putNumber("smallF", bottomFlywheelPIDController.getFF());

        SmartDashboard.putNumber("Top SetPoint RPM", TOP_SETPOINT_RPM);
        SmartDashboard.putNumber("Bottom Setpoint RPM", BOTTOM_SETPOINT_RPM);

        topFlywheel.setIdleMode(IdleMode.kCoast);
        bottomFlywheel.setIdleMode(IdleMode.kCoast);
    }

    public void turnOffFlywheels() {
        topFlywheel.set(0);
        bottomFlywheel.set(0);
    }

    public void setSpeed(double topSpeedRPM, double bottomSpeedRPM) {
        setTopFlywheelSpeed(topSpeedRPM);
        setBottomFlywheelSpeed(bottomSpeedRPM);
    }

    public void setTopFlywheelSpeed(double topSpeedRPM) {
        topFlywheelPIDController.setReference(topSpeedRPM, ControlType.kVelocity);
    }
    
    public void setBottomFlywheelSpeed(double bottomSpeedRPM) {
        bottomFlywheelPIDController.setReference(bottomSpeedRPM, ControlType.kVelocity);
    }

    public double getTopSetpointRPM() {
        return TOP_SETPOINT_RPM;
    }

    public double getBottomSetpointRPM() {
        return BOTTOM_SETPOINT_RPM;
    }

    public double getTopCurrentRPM() {
        return topFlywheel.getEncoder().getVelocity();
    }

    public double getBottomCurrentRPM() {
        return bottomFlywheel.getEncoder().getVelocity();
    }

    public boolean isWithinTolerance() {
        return Math.abs(getTopCurrentRPM() - getTopSetpointRPM()) < RPM_TOLERANCE 
        && Math.abs(getBottomCurrentRPM() - getBottomSetpointRPM()) < RPM_TOLERANCE;
    }

    public void periodic() {
        SmartDashboard.putNumber("Top Current RPM", getTopCurrentRPM());
        SmartDashboard.putNumber("Bottom Current RPM", getBottomCurrentRPM());

    }

    

    
}

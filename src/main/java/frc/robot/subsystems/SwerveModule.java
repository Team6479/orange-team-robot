package frc.robot.subsystems;

import com.ctre.phoenix.sensors.CANCoder;
import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMax.ControlType;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;
import com.revrobotics.RelativeEncoder;
import com.revrobotics.SparkMaxPIDController;

import edu.wpi.first.math.controller.SimpleMotorFeedforward;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.kinematics.SwerveModulePosition;
import edu.wpi.first.math.kinematics.SwerveModuleState;
import frc.robot.Constants;
import frc.robot.util.Conversions;
import frc.robot.util.SwerveModuleConstants;

public class SwerveModule {
    public int moduleNumber;
    private Rotation2d angleOffset;
    private Rotation2d lastAngle;

    private CANSparkMax angleMotor;
    private SparkMaxPIDController angleController;
    private RelativeEncoder angleRelativeEncoder;

    private CANSparkMax driveMotor;
    private SparkMaxPIDController driveController;
    private RelativeEncoder driveRelativeEncoder;

    private CANCoder angleEncoder;

    private SimpleMotorFeedforward feedforward = new SimpleMotorFeedforward(Constants.Swerve.DRIVE_S, Constants.Swerve.DRIVE_V, Constants.Swerve.DRIVE_A);

    public SwerveModule(int moduleNumber, SwerveModuleConstants moduleConstants) {
        this.moduleNumber = moduleNumber;
        this.angleOffset = moduleConstants.angleOffset;
        
        /* Angle Encoder Config */
        angleEncoder = new CANCoder(moduleConstants.cancoderID);
        configAngleEncoder();

        /* Angle Motor Config */
        angleMotor = new CANSparkMax(moduleConstants.angleMotorID, MotorType.kBrushless);
        angleController = angleMotor.getPIDController();
        angleRelativeEncoder = angleMotor.getEncoder();
        configAngleMotor();

        /* Drive Motor Config */
        driveMotor = new CANSparkMax(moduleConstants.driveMotorID, MotorType.kBrushless);
        driveController = driveMotor.getPIDController();
        driveRelativeEncoder = driveMotor.getEncoder();
        configDriveMotor();

        lastAngle = getState().angle;
    }

    public void setDesiredState(SwerveModuleState desiredState, boolean isOpenLoop){
        /* This is a custom optimize function, since default WPILib optimize assumes continuous controller which CTRE and Rev onboard is not */
        desiredState = SwerveModule.optimize(desiredState, getState().angle); 
        setAngle(desiredState);
        setSpeed(desiredState, isOpenLoop);
    }

    private void setSpeed(SwerveModuleState desiredState, boolean isOpenLoop) {
        if (isOpenLoop) {
            double percentOutput = desiredState.speedMetersPerSecond / Constants.Swerve.MAX_SPEED;
            driveMotor.set(percentOutput);
        }
        else {
            double velocity = Conversions.MPSToNeo(desiredState.speedMetersPerSecond, Constants.Swerve.WHEEL_CIRCUMFERENCE, Constants.Swerve.DRIVE_GEAR_RATIO);
            driveController.setReference(velocity, ControlType.kVelocity, 0, feedforward.calculate(desiredState.speedMetersPerSecond));
        }
    }

    private void setAngle(SwerveModuleState desiredState) {
        Rotation2d angle = (Math.abs(desiredState.speedMetersPerSecond) <= (Constants.Swerve.MAX_SPEED * 0.01)) ? lastAngle : desiredState.angle; //Prevent rotating module if speed is less then 1%. Prevents Jittering.
        
        angleController.setReference(Conversions.degreesToNeo(angle.getDegrees(), Constants.Swerve.ANGLE_GEAR_RATIO), ControlType.kPosition);
        lastAngle = angle;
    }

    private Rotation2d getAngle() {
        return Rotation2d.fromDegrees(Conversions.neoToDegrees(angleRelativeEncoder.getPosition(), Constants.Swerve.ANGLE_GEAR_RATIO));
    }

    public double getCanCoderAngle() {
        return getCanCoder().getDegrees()- angleOffset.getDegrees();
    }

    public Rotation2d getCanCoder() {
        return Rotation2d.fromDegrees(angleEncoder.getAbsolutePosition());
    }

    public void resetToAbsolute() {
        double absolutePosition = Conversions.degreesToNeo(getCanCoder().getDegrees() - angleOffset.getDegrees(), Constants.Swerve.ANGLE_GEAR_RATIO);
        angleRelativeEncoder.setPosition(absolutePosition);
    }

    public Rotation2d getAngleSetpoint() {
        return lastAngle;
    }

    public SparkMaxPIDController getDriveController() {
        return driveController;
    }

    public SparkMaxPIDController getAngleController() {
        return angleController;
    }

    private void configAngleEncoder() {        
        angleEncoder.configFactoryDefault();
        angleEncoder.configAllSettings(Constants.Swerve.CTRE_CONFIGS.swerveCanCoderConfig);
    }

    private void configAngleMotor() {
        angleMotor.restoreFactoryDefaults();
        angleMotor.setInverted(Constants.Swerve.INVERT_ANGLE_MOTORS);
        angleMotor.setIdleMode(Constants.Swerve.ANGLE_IDLE_MODE);
        resetToAbsolute();

        angleController.setP(Constants.Swerve.ANGLE_P);
        angleController.setI(Constants.Swerve.ANGLE_I);
        angleController.setD(Constants.Swerve.ANGLE_D);
        angleController.setFF(Constants.Swerve.ANGLE_F);

        angleMotor.setSmartCurrentLimit(Constants.Swerve.ANGLE_CONTINUOUS_CURRENT_LIMIT);
        angleMotor.enableVoltageCompensation(12);
        angleMotor.burnFlash();
    }

    private void configDriveMotor() {        
        driveMotor.restoreFactoryDefaults();
        driveMotor.setInverted(Constants.Swerve.INVERT_DRIVE_MOTORS);
        driveMotor.setIdleMode(Constants.Swerve.DRIVE_IDLE_MODE);
        driveRelativeEncoder.setPosition(0);

        driveController.setP(Constants.Swerve.DRIVE_P);
        driveController.setI(Constants.Swerve.DRIVE_I);
        driveController.setD(Constants.Swerve.DRIVE_D);
        driveController.setFF(Constants.Swerve.DRIVE_F);

        driveMotor.setSmartCurrentLimit(Constants.Swerve.DRIVE_CONTINUOUS_CURRENT_LIMIT);
        driveMotor.setOpenLoopRampRate(Constants.Swerve.OPEN_LOOP_RAMP);
        driveMotor.setClosedLoopRampRate(Constants.Swerve.CLOSED_LOOP_RAMP);
        driveMotor.enableVoltageCompensation(12);
        driveMotor.burnFlash();
    }

    public SwerveModuleState getState() {
        return new SwerveModuleState(
            Conversions.neoToMPS(driveRelativeEncoder.getVelocity(), Constants.Swerve.WHEEL_CIRCUMFERENCE, Constants.Swerve.DRIVE_GEAR_RATIO), 
            getAngle()
        ); 
    }

    public SwerveModulePosition getPosition() {
        return new SwerveModulePosition(
            Conversions.neoToMeters(driveRelativeEncoder.getPosition(), Constants.Swerve.WHEEL_CIRCUMFERENCE, Constants.Swerve.DRIVE_GEAR_RATIO), 
            getAngle()
        );
    }

    public CANSparkMax getAngleMotor() {
        return angleMotor;
    }

    public CANSparkMax getDriveMotor() {
        return driveMotor;
    }

    /**
     * Minimize the change in heading the desired swerve module state would require by potentially
     * reversing the direction the wheel spins. Customized from WPILib's version to include placing
     * in appropriate scope for CTRE onboard control.
     *
     * @param desiredState The desired state.
     * @param currentAngle The current module angle.
     */
    public static SwerveModuleState optimize(SwerveModuleState desiredState, Rotation2d currentAngle) {
        double targetAngle = placeInAppropriate0To360Scope(currentAngle.getDegrees(), desiredState.angle.getDegrees());
        double targetSpeed = desiredState.speedMetersPerSecond;
        double delta = targetAngle - currentAngle.getDegrees();
        if (Math.abs(delta) > 90){
            targetSpeed = -targetSpeed;
            targetAngle = delta > 90 ? (targetAngle -= 180) : (targetAngle += 180);
        }        
        return new SwerveModuleState(targetSpeed, Rotation2d.fromDegrees(targetAngle));
    }

    /**
     * @param scopeReference Current Angle
     * @param newAngle Target Angle
     * @return Closest angle within scope
     */
    private static double placeInAppropriate0To360Scope(double scopeReference, double newAngle) {
        double lowerBound;
        double upperBound;
        double lowerOffset = scopeReference % 360;
        if (lowerOffset >= 0) {
            lowerBound = scopeReference - lowerOffset;
            upperBound = scopeReference + (360 - lowerOffset);
        } else {
            upperBound = scopeReference - lowerOffset;
            lowerBound = scopeReference - (360 + lowerOffset);
        }
        while (newAngle < lowerBound) {
            newAngle += 360;
        }
        while (newAngle > upperBound) {
            newAngle -= 360;
        }
        if (newAngle - scopeReference > 180) {
            newAngle -= 360;
        } else if (newAngle - scopeReference < -180) {
            newAngle += 360;
        }
        return newAngle;
    }
}